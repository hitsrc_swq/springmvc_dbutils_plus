#springmvc_dbutils_plus

   
![打包注意](https://git.oschina.net/uploads/images/2017/0925/150741_a72ed6be_722815.png "打包注意")

打成可运行jar时， **请注意一定要选第一项**，
选择第二项的话，CodeMaker.class.getProtectionDomain().getCodeSource().getLocation() 返回值会是 rsrc:./ 
会导致取不到jar物理路径，运行出错！！！


  功能运行界面

   ![dbutils_plus](https://images.gitee.com/uploads/images/2018/1013/154504_2af096ed_722815.png "优化后的界面")


  ======================================================================================

 ![swt-jface](https://git.oschina.net/uploads/images/2017/0504/163524_e6950195_722815.png "swt-jface")

此为Windows 64位系统eclipse swt—jface包，如为32位系统请自行替换打包。
![运行1](https://images.gitee.com/uploads/images/2018/1013/155232_d252f017_722815.png "222.png")
![运行2](https://images.gitee.com/uploads/images/2018/1013/155242_ea2002d2_722815.png "333.png")
![运行3](https://images.gitee.com/uploads/images/2018/1013/155251_af34789d_722815.png "444.png")

IDE导入生成的maven工程代码

项目运行效果举例
![运行效果](https://images.gitee.com/uploads/images/2018/1013/155404_420a6746_722815.png "5555.png")


欢迎点击链接加入技术讨论群【Java 爱码少年】：https://jq.qq.com/?_wv=1027&k=4AuWuZu